﻿using System;


namespace ListenerTask1
{
    public class FireEventArgs : EventArgs
    {
        public FireEventArgs(int value)
        {
            Value = value;
        }

        public int Value { get; private set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class FireAlarm
    {
        /// <summary>
        /// FireEventHandler
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="fireEventArgs">FireEventArgs with some int value</param>
        public delegate void FireEventHandler(object sender, FireEventArgs fireEventArgs);

        /// <summary>
        /// Fire Event
        /// </summary>
        public static event FireEventHandler FireEvent;

        static FireAlarm()
        {
            FireEvent += OnFireEvent;
            Console.WriteLine("Listen start. OTHER Assembly");
        }

        public static void DoEvent(int value)
        {
            FireEventArgs fireArgs = new FireEventArgs(value);
            FireEvent(null, fireArgs);
        }

        private static void OnFireEvent(object sender, FireEventArgs e)
        {
            Console.WriteLine("Fire!!! Call from OTHER Assembly Value = " + e.Value);
        }
    }
}
