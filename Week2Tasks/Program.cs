﻿using System;
using System.Collections.Generic;


namespace Week2Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            NotifyUser();
            string curLine = string.Empty;
            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey(true);
                if (key.Key == ConsoleKey.Backspace)
                {
                    if (key.Key == ConsoleKey.Backspace && curLine.Length > 0)
                    {
                        curLine = curLine.Substring(0, (curLine.Length - 1));
                        Console.Write("\b \b");
                    }
                }
                else
                {
                    if (key.Key == ConsoleKey.Enter)
                    {
                        int currentTask;
                        if (!int.TryParse(curLine, out currentTask) || !RunAction(currentTask))
                        {
                            Console.WriteLine("\nPlease input correct number!");
                        }
                        curLine = string.Empty;
                    }
                    else
                    {
                        double val;
                        if (double.TryParse(key.KeyChar.ToString(), out val))
                        {
                            curLine += key.KeyChar;
                            Console.Write(key.KeyChar);
                        }
                    }
                }
            }
            while (key.Key != ConsoleKey.Escape);
        }

        private static void NotifyUser()
        {
            Console.WriteLine("Choose any task from 1-13. Or press Esc to exit...");
        }

        private delegate void CurrentAction();

        private static readonly Dictionary<int, CurrentAction> ActionDictionary = new Dictionary<int, CurrentAction>
                            {
                              {1, new CurrentAction(Task1.DoTask)},
                              {2, new CurrentAction(Task2.DoTask)},
                              {3, new CurrentAction(Task3.DoTask)},
                              {4, new CurrentAction(Task4.DoTask)},
                              {5, new CurrentAction(Task5.DoTask)},
                              {6, new CurrentAction(Task6.DoTask)},
                              {7, new CurrentAction(Task7.DoTask)},
                              {8, new CurrentAction(Task8.DoTask)},
                              {9, new CurrentAction(Task9.DoTask)},
                              {10, new CurrentAction(Task10.DoTask)},
                              {11, new CurrentAction(Task11.DoTask)},
                              {12, new CurrentAction(Task12.DoTask)},
                              {13, new CurrentAction(Task13.DoTask)}
                            };

        private static bool RunAction(int currentTask)
        {
            if (ActionDictionary.ContainsKey(currentTask))
            {
                Console.WriteLine();
                ActionDictionary[currentTask].Invoke();
                Console.WriteLine();
                NotifyUser();
                return true;
            }

            return false;
        }
    }
}
