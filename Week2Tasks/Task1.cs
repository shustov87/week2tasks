﻿//1. Create a class that throws an event and derives a custom class based on EventArgs.
//Then create an assembly that responds to the event.

using System;
using ListenerTask1;

namespace Week2Tasks
{
    class Task1
    {
        public static void DoTask()
        {
            Console.WriteLine("FireEvent in another assemly...");
            FireAlarm.FireEvent -= FireEventOccured;
            FireAlarm.FireEvent += FireEventOccured;
            FireAlarm.DoEvent(99);
        }

        private static void FireEventOccured(object sender, FireEventArgs e)
        {
            Console.WriteLine("Fire!!! From main. Value = " + e.Value);
        }
    }
}
