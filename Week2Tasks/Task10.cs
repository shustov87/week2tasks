﻿//10. Create a class with private constructor. 
//Try to create an instance of this class using reflection. 
//(If you know several ways to do this - show them all)

using System;
using System.Reflection;

namespace Week2Tasks
{
    class Task10
    {
        /// <summary>
        /// 
        /// </summary>
        class MyClass
        {
            public int SomeValue { get; set; }

            private readonly string _status = "none";

            private MyClass()
            {
                _status = "Private ctor was called";
                Console.WriteLine(_status);
            }

            private MyClass(int val)
            {
                _status = "Private ctor with parameters was called";
                SomeValue = val;
                Console.WriteLine(_status);
            }

            public override string ToString()
            {
                return _status;
            }
        }

        public static void DoTask()
        {
            Type t = typeof (MyClass);
            Console.WriteLine("Create {0} type instance using reflection", t.Name);
            ConstructorInfo mainCtor = t.GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { }, null);

            ConstructorInfo[] ctorArr = t.GetConstructors(BindingFlags.NonPublic | BindingFlags.Instance);

            MyClass obj1 = (MyClass)mainCtor.Invoke(new Object[] { });
            MyClass obj2 = (MyClass)ctorArr[1].Invoke(new Object[] { 5 });

            Console.WriteLine("Create {0} type instance using Activator", t.Name);
            MyClass obj3 = (MyClass)Activator.CreateInstance(t, true);
        }
    }
}
