﻿//11. Create a custom attribute and mark several methods in your class with it.
// Enumerate methods marked with your attribute using reflection.

using System;
using System.Collections;
using System.Reflection;
using System.Linq;


namespace Week2Tasks
{
    class Task11
    {
        /// <summary>
        /// custom attributes can be apply to any targets
        /// </summary>
        [AttributeUsage(AttributeTargets.All, AllowMultiple = true)  ]
        public class AuthorAttribute : Attribute
        {
            readonly string _name;

            public double Version { get; set; }

            public AuthorAttribute(string name)
            {
                this._name = name;
                Version = 1.0; 
            }
            
            public string GetName()
            {
                return _name;
            }
        }
        
        
        private class TestClass
        {
            public void DoSomething()
            {
                // ...
            }

            [Author("Vasya")]
            public void DoSomething1(int input)
            {}

            [Author("Vasya"), Author("Petro", Version = 2.0)]
            public void DoSomething2(string str1, string str2)
            {
                // ...
            }
        }
        
        public static void DoTask()
        {
            MethodInfo[] methods = typeof(TestClass).GetMethods();

            foreach (MethodInfo method in methods)
            {
                var attrs = Attribute.GetCustomAttributes(method).OfType<AuthorAttribute>().ToList();  // reflection
                if (attrs.Any())
                {
                    Console.WriteLine(method.ToString());
                }

                foreach (AuthorAttribute attr in attrs)
                {
                    Console.WriteLine("\t   [{0}, version {1:f}]", attr.GetName(), attr.Version);
                }
            }
        }
    }
}
