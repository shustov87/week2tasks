﻿//12. Create a simple application which will show the mode it was compiled (Debug/Release)

using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace Week2Tasks
{
    class Task12
    {
        public static void DoTask()
        {
            Console.WriteLine(IsAssemblyDebugBuild(Assembly.GetExecutingAssembly()) ? "Debug" : "Release");
        }
      
        /// <summary>
        /// return true if current assembly run in debug mode
        /// </summary>
        /// <param name="assembly">current assembly</param>
        /// <returns></returns>
        private static bool IsAssemblyDebugBuild(Assembly assembly)
        {
            return assembly.GetCustomAttributes(false).OfType<DebuggableAttribute>().Select(debuggableAttribute => debuggableAttribute.IsJITTrackingEnabled).FirstOrDefault();
        }
    }
}
