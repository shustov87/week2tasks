﻿//13. Create a class "universal constructor" which will allow to create instance of any passed type 
//(even with private constructor).  Additional conditions: type should be a class, you should return 
//null in case if passed type is static, use only default constructors (without parameters). 
//(If you know several ways to do this - show them all) 

using System;

namespace Week2Tasks
{
    class Task13
    {
        class MyClass1
        {
            private readonly int _foo = 1;

            public MyClass1()
            {
                _foo = 9;
            }
            
            public override string ToString()
            {
                return _foo.ToString();
            }
        }

        class MyClass2
        {
            private readonly int _foo = 2;

            private MyClass2()
            {
                _foo = 99;
            }

            public override string ToString()
            {
                return _foo.ToString();
            }
        }

        static class MyClass3
        {
            private static readonly int _foo = 3;

            static MyClass3()
            {
                _foo = 999;
            }
        }

        /// <summary>
        /// Class "universal constructor" which will allow to create instance of any passed type (even with private constructor)
        /// </summary>
        static class SuperConstructor<T>
        {
            /// <summary>
            /// Create class instance without parameters
            /// </summary>
            public static T CreateInstance()
            {
                try
                {
                    return (T)Activator.CreateInstance(typeof(T), true);
                }
                catch (Exception)
                {
                    Console.WriteLine("Something terrible happened!!!");
                    return default(T);
                }
            }
        }

        public static void DoTask()
        {
            Console.WriteLine("Try create instance MyClass1 with public constructor");
            object o1 = SuperConstructor<MyClass1>.CreateInstance();
            Console.WriteLine("property - {0}", o1);

            Console.WriteLine("Try create instance MyClass2 with private constructor");
            object o2 = SuperConstructor<MyClass2>.CreateInstance();
            Console.WriteLine("property - {0}", o2);

            Console.WriteLine("Try create instance static MyClass3");
            Console.WriteLine("We can't use static type in generics");
        }
    }
}
