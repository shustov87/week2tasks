﻿//2. Create a custom class that implements the necessary interfaces to allow an array of the class 
//to be sorted.
using System;
using System.Collections;


namespace Week2Tasks
{
    class Task2
    {
        /// <summary>
        /// Point
        /// </summary>
        private struct Point
        {
            public int X { get; set; }

            public int Y { get; set; }
        }


        /// <summary>
        /// Temperature class, where implement custom compare
        /// </summary>
        public class Temperature : IComparable
        {
            /// <summary>
            /// Temperature 
            /// </summary>
            public int Value { get; set; }

            public int CompareTo(object obj)
            {
                Temperature otherTemperature = obj as Temperature;
                if (otherTemperature != null)
                    return Value.CompareTo(otherTemperature.Value);

                throw new ArgumentException("Object is not a Temperature");
            }

            public static implicit operator int(Temperature currentTemp)
            {
                return currentTemp.Value;
            }

            public override string ToString()
            {
                const char c = (char)176;
                return Value + c.ToString();
            }
        }

        /// <summary>
        /// My own class Vector. method Compare() compares only vector lenght
        /// </summary>
        private class Vector : IComparable
        {
            /// <summary>
            /// StartPoint 
            /// </summary>
            public Point StartPoint { get; set; }

            /// <summary>
            /// EndPoint
            /// </summary>
            public Point EndPoint { get; set; }
            
            public double Lenght 
            {
                get { return Math.Sqrt(Math.Pow(StartPoint.X - EndPoint.X, 2) + Math.Pow(StartPoint.Y - EndPoint.Y, 2)); }
            }

            public int CompareTo(object obj)
            {
                var otherVector= obj as Vector;
                if (otherVector != null)
                {
                    return this.Lenght.CompareTo(otherVector.Lenght);
                }
                
                throw new ArgumentException("Object is not a Vector");
            }
            
            public override string ToString()
            {
                return Lenght.ToString("F2");
            }
        }

        
        public static void DoTask()
        {
            var vectors = new ArrayList();
            var rnd = new Random();

            // Generate 10 random values.
            for (int i = 0; i < 10; i++)
            {
                var temp = new Vector
                                  {
                                      StartPoint = new Point { X = rnd.Next(0, 100), Y = rnd.Next(0, 100) },
                                      EndPoint = new Point { X = rnd.Next(0, 100), Y = rnd.Next(0, 100) }
                                  };
                vectors.Add(temp);
            }

            vectors.Sort();

            foreach (var temp in vectors)
                Console.Write(temp + " ");

            Console.WriteLine();
        }
    }
}
