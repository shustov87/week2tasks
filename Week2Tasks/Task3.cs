﻿// Create a custom class that can be converted to common value types
using System;


namespace Week2Tasks
{
    class Task3
    {
        public static void DoTask()
        {
            Console.WriteLine("We use class from task 2");
            Task2.Temperature temperature = new Task2.Temperature { Value = 10 };
            double temp = temperature;

            Console.WriteLine("initial Temperature value " + temperature);
            Console.WriteLine("converted double value " + temp);
        }
    }
}
