﻿//4. Fill a collection with 10000 items. Enumerate this collection with for and foreach statements.
// Make a speed test for both of operators (you may use StopWatch class to calculate a time).
// Explain the difference of results.

using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace Week2Tasks
{
    class Task4
    {
        public static void DoTask()
        {
            var sw = new Stopwatch(); 
            var collection = new List<int>();
         
            for (int i = 0; i < 10000000; i++)
                collection.Add(1);

            Console.WriteLine("Collection count = " + collection.Count);
            sw.Start();
            for (int i = 0; i < collection.Count; i++)
            {
                int foo = collection[i];
            }
            sw.Stop();
            Console.WriteLine("for {0} msec", sw.ElapsedMilliseconds);

            sw.Reset();
            sw.Start();
            foreach (var elem in collection)
            {
                int foo = elem;
            }
            sw.Stop();
            Console.WriteLine("foreach {0} msec", sw.ElapsedMilliseconds);
        }
    }
}
