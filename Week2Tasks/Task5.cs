﻿//5. In foreach body try to modify collection (add/remove element). 
//Do the same in body of for statement. In which cycle it is possible to do this? Explain why.
using System;


namespace Week2Tasks
{
    class Task5
    {
        public static void DoTask()
        {
            string str = "You can't modify collection in foreach loop";
            Console.WriteLine(str);
        }
    }
}
