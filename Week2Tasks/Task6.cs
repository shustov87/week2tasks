﻿// 6. Create a linked-list generic class that enables you to create a chain of different object types.
using System;
using System.Collections.Generic;


namespace Week2Tasks
{
    class Task6
    {
        public static void DoTask()
        {
            var linkList = new LinkedList<object>();
            linkList.AddLast("this is the first string");
            linkList.AddLast("this is the second string");
            linkList.AddLast(new string[] { "one", "two" });
            linkList.AddLast(100);
            linkList.AddLast(true);

            Console.WriteLine("linkList.Count = " + linkList.Count);
            foreach (var obj in linkList)
            {
                Console.WriteLine("Type: " + obj.GetType() + "\tValue: " + obj);
            }
        }
    }
}
