﻿//7. Create two classes with identical functionality. 
//Use generics for the first class, and cast the second class to Object types. 
//Create a For loop that uses the class over thousands of iterations. 
//Time the performance of both the generic class and the Object-based class to determine which performs 
//better. You can use DateTime.Now.Ticks to measure the time.

using System;
using System.Diagnostics;

namespace Week2Tasks
{
    class Task7
    {
        /// <summary>
        /// Generic class
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        class GenericObj<T>
        {
            T obj;

            public GenericObj(T obj)
            {
                this.obj = obj;
            }

            public override string ToString()
            {
                return obj.ToString();
            }
        }

        /// <summary>
        /// Common class
        /// </summary>
        class CommonObj
        {
            readonly object obj;

            public CommonObj(object obj)
            {
                this.obj = obj;
            }

            public override string ToString()
            {
                return obj.ToString();
            }
        }

        public static void DoTask()
        {
            // create and fill object list
            var stopwatch = new Stopwatch();
            const int count = 1000000;
            var commonObjsArr = new CommonObj[count];
            var genericObjArr = new GenericObj<int>[count];
            for(int i = 0; i < count;i++)
            {
                commonObjsArr[i] = new CommonObj(10);
                genericObjArr[i] = new GenericObj<int>(10);
            }

            // test generic class
            stopwatch.Start();
            foreach (var foo in genericObjArr)
            {
                string str = foo.ToString();
            }
            stopwatch.Stop();
            long genericTicks = stopwatch.ElapsedTicks;
            Console.WriteLine("generic class - " + genericTicks);
            stopwatch.Reset();

            // test common class
            stopwatch.Start();
            foreach (var foo in commonObjsArr)
            {
                string str = foo.ToString();
            }
            stopwatch.Stop();
            long commonTicks = stopwatch.ElapsedTicks;
            Console.WriteLine("common class - " + commonTicks);
            Console.WriteLine(commonTicks > genericTicks ? "generic class faster!!!" : "common class faster!!!");
        }
    }
}
