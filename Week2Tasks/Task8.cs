﻿//8. Imagine that your application use NotifyClient event to notify remote clients. 
//In case if client is disconnected exception will be thrown and other delegates will not be invoke,
//so other clients will not be notified. Implement a method which will fire NotifyClient event and 
//will be free of this problem.

using System;
using System.Collections.Generic;

namespace Week2Tasks
{
    class Task8
    {
        /// <summary>
        /// Class server
        /// </summary>
        class Server
        {
            private event Action<string> NotifyClient;
            //delegate void NotifyClientHandler(string param); // to decrease type fields use FCL type Action<T>

            /// <summary>
            /// ClientList
            /// </summary>
            public static List<Client> ClientList { get; private set; } 

            static Server()
            {
                ClientList = new List<Client>();
            }

            /// <summary>
            /// Add new client and subscribe it to notifications
            /// </summary>
            /// <param name="client">Client</param>
            public void AddClient(Client client)
            {
                ClientList.Add(client);
                this.NotifyClient += client.ReceiveSomething; 
            }

            /// <summary>
            /// Send message to all clients from ClientList 
            /// </summary>
            /// <param name="message">message</param>
            public void NotifyClients(string message)
            {
                foreach (Action<string> d in NotifyClient.GetInvocationList())
                {
                    try
                    {
                        d(message);
                    }
                    catch (Exception ex)
                    {
                        var client = d.Target as Client;
                        string name = client != null? client.Name : string.Empty;
                        Console.WriteLine("\tSome exception has occurred with " + name);
                    }
                }
            }
        }
        
        /// <summary>
        /// Client Class 
        /// </summary>
        class Client
        {
            public bool IsStupidClient { get; private set; }

            public string Name { get; private set; }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="name">client name</param>
            /// <param name="isStupidClient">if isStupidClient - throw error during get message from server</param>
            public Client(string name, bool isStupidClient)
            {
                IsStupidClient = isStupidClient;
                Name = name;
            }

            /// <summary>
            /// Action when get message
            /// </summary>
            /// <param name="str"></param>
            public void ReceiveSomething(string str)
            {
                if (IsStupidClient)
                    throw new Exception();

                Console.WriteLine("Client ({0}) received some message: {1}", Name, str);
            }
        }
        
        public static void DoTask()
        {
            var server = new Server();
            server.AddClient(new Client("Client1", false));
            server.AddClient(new Client("Client2", true));
            server.AddClient(new Client("Client3", false));
            server.NotifyClients("hello");
        }
    }
}
