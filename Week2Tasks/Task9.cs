﻿//9. Create a static class with couple of private methods. 
//One of these methods should be generic. 
//Try to invoke these methods using reflection.

using System;
using System.Reflection;

namespace Week2Tasks
{
    class Task9
    {
        static class MyClass
        {
            private static void DoSomething()
            {
                Console.WriteLine("Private method {0}() was called", MethodBase.GetCurrentMethod().Name);
            }

            public static void PublicGenericMethod<T>(T value1, T value2)
            {
                Console.WriteLine("Public generic method was called");
                Console.WriteLine("Type of value " + typeof(T));
            }

            private static void PrivateGenericMethod<T>(T value)
            {
                Console.WriteLine("Private generic method was called");
                Console.WriteLine("Type of value " + value.GetType());
            }
        }

        public static void DoTask()
        {
            // Call private method
            MethodInfo mi1 = typeof(MyClass).GetMethod("DoSomething", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
            mi1.Invoke(null, null);

            // Call public generic method
            MethodInfo mi2 = typeof(MyClass).GetMethod("PublicGenericMethod");
            MethodInfo miConstructed = mi2.MakeGenericMethod(typeof(int));
            object[] args = { 42, 24 };
            miConstructed.Invoke(null, args);

            // Call private generic method
            MethodInfo mi3 = typeof(MyClass).GetMethod("PrivateGenericMethod", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
            MethodInfo gm = mi3.MakeGenericMethod(typeof(int));
            object[] args1 = { 42 };
            gm.Invoke(null, args1);
        }
    }
}
