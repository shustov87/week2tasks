﻿1. Create a class that throws an event and derives a custom class based on EventArgs. Then create an 
assembly that responds to the event.

2. Create a custom class that implements the necessary interfaces to allow an array of the class to 
be sorted.

3. Create a custom class that can be converted to common value types

4. Fill a collection with 10000 items. Enumerate this collection with for and foreach statements.
 Make a speed test for both of operators (you may use StopWatch class to calculate a time).
 Explain the difference of results.

5. In foreach body try to modify collection (add/remove element). 
Do the same in body of for statement. In which cycle it is possible to do this? Explain why.

6. Create a linked-list generic class that enables you to create a chain of different object types.

7. Create two classes with identical functionality. 
Use generics for the first class, and cast the second class to Object types. 
Create a For loop that uses the class over thousands of iterations. 
Time the performance of both the generic class and the Object-based class to determine which performs 
better. You can use DateTime.Now.Ticks to measure the time.

8. Imagine that your application use NotifyClient event to notify remote clients. 
In case if client is disconnected exception will be thrown and other delegates will not be invoke,
so other clients will not be notified. Implement a method which will fire NotifyClient event and 
will be free of this problem.

9. Create a static class with couple of private methods. 
One of these methods should be generic. 
Try to invoke these methods using reflection.

10. Create a class with private constructor. 
Try to create an instance of this class using reflection. 
(If you know several ways to do this - show them all)

11. Create a custom attribute and mark several methods in your class with it.
 Enumerate methods marked with your attribute using reflection.

12. Create a simple application which will show the mode it was compiled (Debug/Release)

13. Create a class "universal constructor" which will allow to create instance of any passed type 
(even with private constructor).  Additional conditions: type should be a class, you should return 
null in case if passed type is static, use only default constructors (without parameters). 
(If you know several ways to do this - show them all) 